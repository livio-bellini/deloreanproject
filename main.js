//LOADING SPINNER SUSPENSION
let loading = document.querySelector('#spinner');
function spinner() {
    setTimeout(function () {
        loading.classList.remove('show');
    }, 230);
};
spinner();
//LOADING SPINNER SUSPENSION

//NAVBAR SHINING
let navBar = document.querySelector('.navbar');
document.addEventListener('scroll', () => {
    let scrolled = window.scrollY;
    if(scrolled == 0) {
        navBar.classList.remove('bg-black');
        navBar.classList.add('navBarShine');
    } else {
        navBar.classList.remove('navBarShine');
        navBar.classList.add('bg-black');
    }
});
//NAVBAR SHINING

//SLIDESHOW HEADER
let slideIndex = 0;
showSlides();
function showSlides() {
    let i;
    let slides = document.querySelectorAll(".mySlides");
    let dots = document.querySelectorAll(".dot");
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
    }
    slideIndex++;
    if (slideIndex > slides.length) {
        slideIndex = 1;
    }    
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" acTive ", "");
    }
    slides[slideIndex-1].style.display = "block";  
    dots[slideIndex-1].className += " acTive ";
    setTimeout(showSlides, 8000);  //Change image every 8 seconds.
  }
//SLIDESHOW HEADER

//SLIDE A CONTENT FROM RIGHT
let ANIMATEDSLIDE = document.querySelector('#animatedSlide > *');
function handleIntersection1(entries1){
    entries1.map(entry => {
        if(entry.isIntersecting){
            ANIMATEDSLIDE.classList.add('slideFromRight');
        }
    });
}
let observerAnimatedSlide = new IntersectionObserver(handleIntersection1);
observerAnimatedSlide.observe(ANIMATEDSLIDE);
//SLIDE A CONTENT FROM RIGHT

//SLIDE MULTIPLE CONTENTS
let BOXES = document.querySelectorAll('#boxEs > *');
function handleIntersection2(entries2){
    entries2.map((entry, index) => {
        if(entry.isIntersecting){
            entry.target.classList.remove('opacity-0');  
            entry.target.classList.add('slideFromDown');            
            entry.target.style.animationDelay = `${index++}s`;
        }
    });
}
let observerBoxEs = new IntersectionObserver(handleIntersection2);
BOXES.forEach(element => {
    observerBoxEs.observe(element);
});
//SLIDE MULTIPLE CONTENTS

//CREATION CARD FROM JSON FILE WITH INTEGRATED FILTERING
fetch('./annunci.json')                                        //READ DATA FROM JSON FILE
    .then((response) => response.json())
    .then((data) => {
        let countries = [];                                            //MAKE EMPTY ARRAY
        let rowBox = document.querySelector('#rowBox');
        let searchBox = document.querySelector('#searchBox');
        let priceLabel = document.querySelector('#priceLabel');
        let priceRange = document.querySelector('#priceRange');
        let selectCountries = document.querySelector('#selectCountries');
        let btnSearch = document.querySelector('#btnSearch');
        let alert = document.querySelector('#alert');

        //FUNCTION TO FILL THE EMPTY ARRAY WITH “element.country” FOR CATEGORIES CREATION
        function setCountries(){
            data.forEach(element => {
                if(!countries.includes(element.country)) {
                    countries.push(element.country);
                }
            })
        }
        setCountries();

        //FUNCTION FOR SORTING AND MAKING OPTIONS FOR THE MENU WITH ID: “selectCountries”
        countries.sort().forEach(element => {
            let option = document.createElement('option');
            option.setAttribute('value', element);
            option.innerHTML = element;
            selectCountries.appendChild(option);
        });

        //FUNCTION TO CREATE THE CARDS
        function createCards(array){
            rowBox.innerHTML = '';
            array.forEach(element => {
                let div = document.createElement('div');
                div.classList.add('col-lg-2', 'col-sm-6', 'col-12', 'my-3');
                div.innerHTML = `
                    <a class="cat-item d-block bg-light text-center rounded p-3" href="">
                        <div class="rounded p-4">
                            <div class="icon mb-3">
                                <img class="img-fluid" src="./img/DMC-BlackLogo.png" alt="DMC Logo">
                            </div>
                            <h6>${element.country}</h6>
                            <h6>${element.city}</h6>
                            <h6>${element.color_car}</h6>
		        <h6>${element.price} $</h6>
                        </div>
                    </a>
                `;
                rowBox.appendChild(div);
            });
        }

        //FUNCTION TO CREATE FILTERING BY WORD FOR THE INPUT FORM WITH ID: "searchBox"
        function filterByWord(array) {
            let filtered = array.filter(element => element.color_car.toLowerCase().includes(searchBox.value.toLowerCase()));
            return filtered;
        }
        
        //FUNCTION TO SETUP THE MAXIMUM PRICE
        function setMaxPrice() {
            let priceArray = data.map(element => Number(element.price));
            let maxPrice = Math.max(...priceArray);
            priceRange.max = maxPrice;
            priceRange.value = maxPrice;
            priceLabel.innerHTML = `Search by price: ${maxPrice} $`;
            priceLabel.innerHTML = `Search by price: ${priceRange.value} $`;  
        }

        //FUNCTION TO CREATE FILTERING BY PRICE FOR THE BAR WITH ID: "priceRange"
        function filterByPrice(array) {
            let filtered = array.filter(element => Number(element.price) <= Number(priceRange.value));
            return filtered;           
        }
    
        //FUNCTION TO CREATE FILTERING BY CATEGORY FOR THE MENU WITH ID: "selectCountries"
        function filterByCategory(array) {
            if(selectCountries.value == 'all') {
                return array;
            } else {
                let filtered = array.filter(element => element.country == selectCountries.value);
                return filtered;
            }
        }

        //FUNCTION FOR THE GLOBAL FILTER
        function globalFilter() {
            let resultFilterWord = filterByWord(data);
	        let resultFilterPrice = filterByPrice(resultFilterWord);
            let resultFilterCategory = filterByCategory(resultFilterPrice);
	        if (resultFilterCategory.length > 0) {
       		    alert.classList.add('d-none');
	            createCards(resultFilterCategory);
	        } else {
		        alert.classList.remove('d-none');
		        createCards(resultFilterCategory);
            }
        }

        //FUNCTION TO MAKE ACTIVE THE SEARCH BUTTON WITH ID: “btnSearch”
        btnSearch.addEventListener('click', () => {
	        globalFilter();
        });

        //FUNCTION TO CREATE DYNAMIC PRICE
        priceRange.addEventListener('input', () => {
            priceLabel.innerHTML = `Search by price:’ ${priceRange.value} $`;
        });
        setMaxPrice();
        createCards(data);   
    });
//CREATION CARD FROM JSON FILE WITH INTEGRATED FILTERING

//CREATION OF DYNAMIC COUNTERS ON SCREEN WITH START ON INTERSECTION OBSERVER
const dataCounter = [2413, 4267, 1988, 1140, 1233];
let rowCounter = document.querySelector('#rowCounter');
let labelCounter = document.querySelectorAll('.labelCounter');
function handleIntersection3(entries3){ 
entries3.map((entry) => {
    if(entry.isIntersecting){
        for(let i = 0; i < dataCounter.length; i++) {
            let counter = 0;
            let interval = setInterval(() => {
                counter++;
                labelCounter[i].innerHTML = `${counter}`;
                if (counter >= dataCounter[i]) {
                    counter = 0;
                    clearInterval(interval);
                }
            }, 1);
        }        
    }});
}
let observerrowCounter = new IntersectionObserver(handleIntersection3);
observerrowCounter.observe(rowCounter);
//CREATION OF DYNAMIC COUNTERS ON SCREEN WITH START ON INTERSECTION OBSERVER

